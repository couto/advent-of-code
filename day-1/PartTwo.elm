{-
   --- Day 1: No Time for a Taxicab ---

   --- Part Two ---

   Then, you notice the instructions continue on the back of the Recruiting
   Document. Easter Bunny HQ is actually at the first location you visit twice.

   For example, if your instructions are R8, R4, R4, R8, the first location you
   visit twice is 4 blocks away, due East.

   How many blocks away is the first location you visit twice?
-}


module PartTwo exposing (..)

import PartOne exposing (..)
import Html exposing (Html, text)


--- Calculate every stop we make according to the instructions


stops : Instruction -> List Position -> List Position
stops instruction positions =
    positions
        |> List.head
        |> Maybe.map (walk instruction)
        |> Maybe.map (\position -> position :: positions)
        |> Maybe.withDefault [ initialPosition ]



--- Calculate every step needed to take between two stops


stepsBetween : Position -> Position -> List Position
stepsBetween begin end =
    let
        horizontal =
            range begin.x end.x

        vertical =
            range begin.y begin.y
    in
        if List.length horizontal > List.length vertical then
            List.map (\i -> { begin | x = i }) horizontal
        else
            List.map (\i -> { begin | y = i }) vertical



--- Create a list of numbers in the given range


range : Int -> Int -> List Int
range begin end =
    if begin > end then
        begin
            |> List.range end
            |> List.reverse
    else
        List.range begin end


steps : Position -> List Position -> List Position
steps position acc =
    acc
        |> Debug.log "acc"
        |> List.head
        |> Maybe.map (stepsBetween position)
        |> Maybe.map (flip List.append acc)
        |> Maybe.withDefault [ position ]


initialPosition : Position
initialPosition =
    { x = 0, y = 0, orientation = North }


main : Html msg
main =
    instructions
        |> List.foldr stops [ initialPosition ]
        |> List.foldr steps []
        |> toString
        |> text
